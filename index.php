<?php

class DES
{
    // Tabelul initial de permutare
    private $IP = array(
        58, 50, 42, 34, 26, 18,
        10, 2, 60, 52, 44, 36, 28, 20,
        12, 4, 62, 54, 46, 38,
        30, 22, 14, 6, 64, 56,
        48, 40, 32, 24, 16, 8,
        57, 49, 41, 33, 25, 17,
        9, 1, 59, 51, 43, 35, 27,
        19, 11, 3, 61, 53, 45,
        37, 29, 21, 13, 5, 63, 55,
        47, 39, 31, 23, 15, 7
    );

    // Tabelul de permutare inițială inversă
    private $IP1 = array(
        40, 8, 48, 16, 56, 24, 64,
        32, 39, 7, 47, 15, 55,
        23, 63, 31, 38, 6, 46,
        14, 54, 22, 62, 30, 37,
        5, 45, 13, 53, 21, 61,
        29, 36, 4, 44, 12, 52,
        20, 60, 28, 35, 3, 43,
        11, 51, 19, 59, 27, 34,
        2, 42, 10, 50, 18, 58,
        26, 33, 1, 41, 9, 49,
        17, 57, 25
    );

    // Tabelul de eliminare a fiecarui 8 bit a cheii initiale
    private $PC1 = array(
        57, 49, 41, 33, 25, 17, 9, 1,
        58, 50, 42, 34, 26, 18, 10, 2,
        59, 51, 43, 35, 27, 19, 11, 3,
        60, 52, 44, 36, 63, 55, 47, 39,
        31, 23, 15, 7, 62, 54, 46, 38,
        30, 22, 14, 6, 61, 53, 45, 37,
        29, 21, 13, 5, 28, 20, 12, 4
    );

    // Tabel de permutare a cheilor pentru fiecare runda
    private $PC2 = array(
        14, 17, 11, 24, 1, 5, 3,
        28, 15, 6, 21, 10, 23, 19, 12,
        4, 26, 8, 16, 7, 27, 20, 13, 2,
        41, 52, 31, 37, 47, 55, 30, 40,
        51, 45, 33, 48, 44, 49, 39, 56,
        34, 53, 46, 42, 50, 36, 29, 32
    );

    // Tabel D-box de expansiune
    private $EP = array(
        32, 1, 2, 3, 4, 5, 4,
        5, 6, 7, 8, 9, 8, 9, 10,
        11, 12, 13, 12, 13, 14, 15,
        16, 17, 16, 17, 18, 19, 20,
        21, 20, 21, 22, 23, 24, 25,
        24, 25, 26, 27, 28, 29, 28,
        29, 30, 31, 32, 1
    );

    // Tabelul permutarilor de compresie
    private $P = array(
        16, 7, 20, 21, 29, 12, 28,
        17, 1, 15, 23, 26, 5, 18,
        31, 10, 2, 8, 24, 14, 32,
        27, 3, 9, 19, 13, 30, 6,
        22, 11, 4, 25
    );

    // Tabelul S-Box
    private $sbox = array(
        array(
            array(14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7),
            array(0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8),
            array(4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0),
            array(15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13)
        ),
        array(
            array(15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10),
            array(3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5),
            array(0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15),
            array(13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9)
        ),
        array(
            array(10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8),
            array(13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1),
            array(13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7),
            array(1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12)
        ),
        array(
            array(7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15),
            array(13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9),
            array(10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4),
            array(3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14)
        ),
        array(
            array(2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9),
            array(14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6),
            array(4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14),
            array(11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3)
        ),
        array(
            array(12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11),
            array(10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8),
            array(9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6),
            array(4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13)
        ),
        array(
            array(4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1),
            array(13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6),
            array(1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2),
            array(6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12)
        ),
        array(
            array(13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7),
            array(1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2),
            array(7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8),
            array(2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11)
        )
    );

    // Numarul de biți al cheii deplasați per rundă
    private $shiftBits = array(1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1);

    // Conversia din sir hexazecimal in sir binar
    private function hexToBin(string $input)
    {
        $binStr = '';

        $hexMap = array(
            '0' => "0000",
            '1' => "0001",
            '2' => "0010",
            '3' => "0011",
            '4' => "0100",
            '5' => "0101",
            '6' => "0110",
            '7' => "0111",
            '8' => "1000",
            '9' => "1001",
            'A' => "1010",
            'B' => "1011",
            'C' => "1100",
            'D' => "1101",
            'E' => "1110",
            'F' => "1111"
        );

        foreach (str_split(strtoupper($input)) as $hexChar) {
            // pentru fiecare caracter hexazecimal convertam valoarea sa
            // concatenam la sirul principal
            $binStr .= $hexMap[$hexChar];
        }

        return $binStr;
    }

    // Conversia din sir binar in sir hexazecimal
    private function binToHex(string $input)
    {
        $hexStr = '';

        $binMap = array(
            "0000" => '0',
            "0001" => '1',
            "0010" => '2',
            "0011" => '3',
            "0100" => '4',
            "0101" => '5',
            "0110" => '6',
            "0111" => '7',
            "1000" => '8',
            "1001" => '9',
            "1010" => 'A',
            "1011" => 'B',
            "1100" => 'C',
            "1101" => 'D',
            "1110" => 'E',
            "1111" => 'F'
        );

        // parcurgem sirul binar cu pasul 4
        for ($i = 0; $i < strlen($input); $i += 4) {
            // formam sirul binar format din 4 biti
            $binVal = '';
            $binVal .= $input[$i];
            $binVal .= $input[$i + 1];
            $binVal .= $input[$i + 2];
            $binVal .= $input[$i + 3];

            // convertam sirul binar in hexazecimal
            // concatenam in sirul principal
            $hexStr .= $binMap[$binVal];
        }

        return $hexStr;
    }

    // permutam sirul hexazecimal de intrare
    // conform secventei specificate
    private function permutation(array $sequence, string $input)
    {
        $output = "";

        $input = str_split($this->hexToBin($input));

        for ($i = 0; $i < count($sequence); $i++) {
            $output .= $input[$sequence[$i] - 1];
        }

        return $this->binToHex($output);
    }

    // realizam operatia logica XOR asupra a doua siruri hexazecimale
    private function xor(string $a, string $b)
    {
        // transformam sirurile in zecimal
        $dec_a = hexdec($a);
        $dec_b = hexdec($b);

        // realizam operatia XOR
        $x = $dec_a ^ $dec_b;

        // transformam rezultatul obtinut din zecimal in hexazecimal
        $a = dechex($x);

        // plasam zerouri in fata pentru a mentine lungimea necesara
        while (strlen($a) < strlen($b)) {
            $a = "0" . $a;
        }

        return $a;
    }

    // deplasare circulara la stanga a bitilor
    private function leftCircularShift(string $input, int $numBits)
    {
        $n = strlen($input) * 4;
        $perm = array();

        for ($i = 0; $i < $n - 1; $i++) {
            $perm[$i] = ($i + 2);
        }

        $perm[$n - 1] = 1;
        while ($numBits-- > 0) {
            $input = $this->permutation($perm, $input);
        }

        return $input;
    }

    // pregatim 16 chei pentru 16 runde
    private function getKeys(string $key)
    {
        $keys = array();

        // realizarea permutatiei initiale a cheii
        $key = $this->permutation($this->PC1, $key);

        // pentru fiecare runda generam cheia respectiva
        for ($i = 0; $i < 16; $i++) {
            $key = $this->leftCircularShift(substr($key, 0, 7), $this->shiftBits[$i])
                . $this->leftCircularShift(substr($key, 7, 14), $this->shiftBits[$i]);

            // Permutam cheia conform tabelei de permutari a cheii
            $keys[$i] = $this->permutation($this->PC2, $key);
        }

        return $keys;
    }

    // Cautarea in tabela S-BOX
    private function sBox(string $input)
    {
        $output = "";

        // transformam sirul hexazecimal in binar
        $input = $this->hexToBin($input);

        for ($i = 0; $i < 48; $i += 6) {
            $temp = substr($input, $i, $i + 6);

            // pregatim indecsii de cautare in S-BOX
            $num = intval($i / 6);
            $row = intval($temp[0] . $temp[5], 2);
            $col = intval(substr($temp, 1, 4), 2);

            // concatenam valoarea gasita in S-BOX la sirul principal
            $output .= dechex($this->sbox[$num][$row][$col]);
        }

        return $output;
    }

    // realizarea fiecarei runde de criptare
    private function round(string $input, string $key, int $num)
    {
        // impartim sirul in doua parti
        $left = substr($input, 0, 8);
        $temp = substr($input, 8, 16);

        $right = $temp;

        // Permutarea de expansiune
        $temp = $this->permutation($this->EP, $temp);

        // Realizam operatia de XOR asupra partii drepte si a cheii
        $temp = $this->xor($temp, $key);

        // Cautam in tabela S-BOX
        $temp = $this->sBox($temp);

        // Permutarea utilizand tabela D-Box
        $temp = $this->permutation($this->P, $temp);

        // Realizam operatia de XOR asupra partii stangi si a partii drepti
        $left = $this->xor($left, $temp);

        // Concatenam partea dreapta a sirului cu partea stanga
        return $right . $left;
    }

    // metoda de criptare a textului
    public function encrypt(string $fullText, string $key)
    {
        $finalText = '';

        // impartim textul in blocuri a cate 64 biti
        // fiecare bloc are maximum 16 caractere hexazecimale
        $textBlocks = str_split($fullText, 16);

        // generam cele 16 cheii pentru fiecare runda
        $keys = $this->getKeys($key);

        // criptam fiecare bloc in parte
        foreach ($textBlocks as $plainText) {
            // umplem textul cu zerouri la urma daca are mai putin de 16 caractere hexazecimale
            // pentru a mentine lungimea constanta de 64 biti
            $plainText = $plainText . str_repeat('0', 16 - strlen($plainText));

            // permutarea initiala
            $plainText = $this->permutation($this->IP, $plainText);

            // trecerea textului prin cele 16 runde de criptare utilizand cheia respectiva
            for ($i = 0; $i < 16; $i++) {
                $plainText = $this->round($plainText, $keys[$i], $i);
            }

            // Schimbarea textului cu locul a cate 32 biti fiecare
            $plainText = substr($plainText, 8, 16) . substr($plainText, 0, 8);

            // Realizarea ultimei permutari
            $plainText = $this->permutation($this->IP1, $plainText);

            // Concatenam textul criptat la sirul final
            $finalText .= $plainText;
        }

        return $finalText;
    }

    // metoda de decriptare
    public function decrypt(string $fullText, string $key)
    {
        $finalText = '';

        // impartim textul in blocuri a cate 64 biti
        // fiecare bloc are maximum 16 caractere hexazecimale
        $textBlocks = str_split($fullText, 16);

        // generam cele 16 cheii pentru fiecare runda
        $keys = $this->getKeys($key);

        // decriptam fiecare bloc in parte
        foreach ($textBlocks as $plainText) {
            // permutarea initiala
            $plainText = $this->permutation($this->IP, $plainText);

            // trecerea textului prin cele 16 runde de criptare utilizand cheia respectiva
            // in ordine inversa
            for ($i = 15; $i > -1; $i--) {
                $plainText = $this->round($plainText, $keys[$i], 15 - $i);
            }

            // Schimbarea textului cu locul a cate 32 biti fiecare
            $plainText = substr($plainText, 8, 16) . substr($plainText, 0, 8);

            // Realizarea ultimei permutari
            $plainText = $this->permutation($this->IP1, $plainText);

            // Concatenam textul criptat la sirul final
            $finalText .= $plainText;
        }

        return $finalText;
    }
}

$text = bin2hex('Lorem Ipsum is simply dummhe 1500s, when fd:./*/*nter took a galley of type and scrambled it to makved not only five centuries, but alfsdf$@&*so the leap into electronic typesetting, remaining esgfdgdfd.');

$key = bin2hex("mDsa)gk");

if (strlen($key) > 16) {
    echo "Key length is too long, enter maximum 8 bytes\n";
    exit;
}

// Daca cheia este mai mica de 64 biti adaugam zerouri la urma pentru a respecta lungimea necesara
$key = $key . str_repeat('0', 16 - strlen($key));

$cipher = new DES();

// criptam textul
$text = $cipher->encrypt($text, $key);

// decriptam textul
$decryptedHEX = $cipher->decrypt($text, $key);

echo "Textul criptat: $text\n\n";

echo "Textul decriptat in hexazecimal: $decryptedHEX \n\n";

echo "Textul decriptat in clar: " . hex2bin($decryptedHEX) . "\n";
